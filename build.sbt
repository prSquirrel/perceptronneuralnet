name := """Neural Network"""

version := "0.0.1"

scalaVersion := "2.11.8"

libraryDependencies  ++= Seq(
  // other dependencies here
  "org.scalanlp" %% "breeze" % "0.12",
  // native libraries are not included by default. add this if you want them (as of 0.7)
  // native libraries greatly improve performance, but increase jar sizes.
  // It also packages various blas implementations, which have licenses that may or may not
  // be compatible with the Apache License. No GPL code, as best I know.
  "org.scalanlp" %% "breeze-natives" % "0.12",
  // the visualization library is distributed separately as well.
  // It depends on LGPL code.
    "org.scalanlp" %% "breeze-viz" % "0.12"
)

initialCommands in console := "import breeze.linalg._ ; import breeze.numerics._"

//libraryDependencies += "org.spire-math" %% "spire" % "0.11.0"
//libraryDependencies +=  "com.typesafe.akka" %% "akka-actor" % "2.4.2"
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.1"

libraryDependencies +=
  "org.scala-lang.modules" %% "scala-pickling" % "0.10.1"

//scalacOptions += "-Xlog-implicits"
//libraryDependencies += "org.apfloat" % "apfloat" % "1.8.2"
//mainClass in (Compile, run) := Some("simple.Network")
fork in run := true
javaOptions in run ++= Seq("-Xmx1G")//, "--XX:+UseG1GC")//, "-Dscala.concurrent.context.numThreads=8", "-Dscala.concurrent.context.maxThreads=8")
