import collection.immutable.TreeMap
import collection.breakOut


object Bigram {
  // Builds a map of all bigrams (aa,ab,ac,...)
  // and corresponding frequencies (0.0)
  def zeros(alphabet: Seq[Char]): TreeMap[String, Double] = {
    (for {
      x <- alphabet
      y <- alphabet
    } yield s"$x$y" -> 0.0)(breakOut)
  }

  // returns the mapping of each bigram to occurence frequency
  def frequency(str: String): Map[String, Double] = {
    str
      .sliding(2)
      .to
      .groupBy(identity _)
      .map(p => (p._1, p._2.length.toDouble))
  }

}
