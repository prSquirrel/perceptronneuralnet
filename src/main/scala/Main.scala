import breeze.linalg._
import breeze.numerics._
import breeze.plot._
import spire.implicits.{cfor}
import breeze.linalg.functions._

import java.nio.file.{Paths, Files}
import scala.util.{Try}
import mlp._

import collection.immutable.TreeMap
import collection.breakOut

import com.github.tototoshi.csv._
import java.io.File
import util.Random

object Main extends App {
  val alphabet = ('a' to 'z') ++ Seq('ą','č','ė','š','ū','ž')

  type In = Seq[String]
  type Out = (List[Double], (Double, Double, Double))
  //transforms a name and nationality
  //into matrix form, suitable for classification
  private val buildMatrices: PartialFunction[In, Out] = {
    case Seq(name, nat) =>
      val totalFreqs = Bigram.zeros(alphabet) ++ Bigram.frequency(name)
      val target = nat match {
        case "lt" => (1.0, 0.0, 0.0)
        case "pl" => (0.0, 1.0, 0.0)
        case "ru" => (0.0, 0.0, 1.0)
      }
      (totalFreqs.values.toList, target)
  }

  def loadOrCreate(filename: String, save: Boolean)(body: => Network): Network = {
    val tryUnpickle: Try[Network] = {
      println(s"Loading from ${filename}...")
      Network.unpickleFromBinary(filename)
    }

    tryUnpickle.getOrElse {
      val net = body
      if (save) {
        println(s"Saving to ${filename}...")
        Network.pickleToBinary(net, filename)
      }
      net
    }
  }


  val trainCSV =
    CSVReader.open(new File("training_set.csv")).iterator.map(buildMatrices).toArray
  val (trainData, trainTarget) =
    Util.shuffleData(DenseMatrix(trainCSV.map(_._1): _*), DenseMatrix(trainCSV.map(_._2): _*))

  val testCSV =
    CSVReader.open(new File("validation_set.csv")).iterator.map(buildMatrices).toArray
  val (testData, testTarget) =
    Util.shuffleData(DenseMatrix(testCSV.map(_._1): _*), DenseMatrix(testCSV.map(_._2): _*))

  val mbSize = 46
  //TODO: pad dataset if it doesn't exactly fit minibatch size
  if( (trainData.rows != trainTarget.rows) || (testData.rows != testTarget.rows) )
    throw new IllegalArgumentException("Input size must match expected output.")

  println(s"""
    Splitting training dataset into ${trainTarget.rows/mbSize} parts
    and test dataset into ${testTarget.rows/mbSize} parts...""")
  val trainSet = for {
    r <- mbSize to trainTarget.rows by mbSize
    range = (r - mbSize) until r
  } yield ( trainData(range, ::), trainTarget(range, ::) )

  val testSet = for {
    r <- mbSize to testTarget.rows by mbSize
    range = (r - mbSize) until r
  } yield ( testData(range, ::), testTarget(range, ::) )


  val network = loadOrCreate("nn-0.bin", save = true) {
      println("Creating new network...")
      val net = new Network(List(1024,100,3), mbSize, regression = false)
      net.evaluate(trainSet, testSet, epochs = 100, shuffle = true, evalTrain = false, evalTest = true)
  }

  println("Processing real data...")

  //extract name column from dataset
  def names = CSVReader.open(new File("db.csv")).toStream.drop(1).collect {
    case List(_,_,name,_,_,_,_) =>
      //remove all non-letter characters
      //because there are LAST names like
      //"el-samar", "SANDLER VINTERMANN" (really?)
      name.replaceAll("""[^\p{L}]+""", "").toLowerCase
  }

  val bigramFreqs = names
    //traverse lazily, so that app doesn't run out of memory
    .iterator
    //generate frequency Map
    .map(Bigram.frequency)
    .map(Bigram.zeros(alphabet) ++ _)
    .map(_.values.toList)
    //group into minibatches
    .grouped(mbSize)
    //just skip data which doesn't fit the group
    .filter(_.lengthCompare(mbSize) == 0)
    //compute
    .map { s: Seq[Seq[Double]] =>
      network.forwardPropagate(DenseMatrix(s: _*))
    }
    //take result with the highest confidence
    .flatMap { out =>
      argmax( out(*, ::) ).toScalaVector.map {
        case 0 => "lt"
        case 1 => "pl"
        case 2 => "ru"
      }
    }
    .zip(names.iterator)

  
  val writer = CSVWriter.open(new File("neuralOut.csv"))
  bigramFreqs.foreach {
    case (res, name) =>
      writer.writeRow(List(name, res))
//      writer.flush()
  }
  writer.close()
}
