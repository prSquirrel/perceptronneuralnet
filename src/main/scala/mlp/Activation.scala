package mlp

import breeze.linalg.{Vector => _, _}
import breeze.numerics._


object Activation {
  type DMD = DenseMatrix[Double]

  def sigmoid(x: DMD, deriv: Boolean = false): DMD = {
    if (!deriv) 1.0 / (exp(-x) + 1.0)
    else {
      val y = sigmoid(x, false)
      (1.0 - y) :* y
    }
  }

  def ctanh(x: DMD, deriv: Boolean = false): DMD = {
    if (!deriv) {
      val y = breeze.numerics.tanh( (2.0 / 3.0) :* x )
      1.7159 :* y
    }
    else {
      val y = breeze.numerics.sech( (2.0/3.0) :* x )
      1.14393 :* (y :* y)
    }
  }

  def identity(x: DMD, deriv: Boolean = false): DMD = {
    if (!deriv) x
    else {
      DenseMatrix.ones[Double](x.rows, x.cols)
    }
  }

  def rbf(x: DMD, deriv: Boolean = false): DMD = {
    if (!deriv) exp( -(x :* x) )
    else {
      -2.0 :* x :* rbf(x, false)
    }
  }

//http://stackoverflow.com/questions/36279904/softmax-derivative-in-numpy-approaches-0-implementation?rq=1
//http://stats.stackexchange.com/questions/79454/softmax-layer-in-a-neural-network
  def softmax(x: DMD, deriv: Boolean = false): DMD = {
    //if (!deriv) {
      val logTerm = breeze.linalg.softmax( x(*,::) )
      exp( x(::,*) - logTerm)
    //}
    // else {
    //   //this is wrong
    //   val y = softmax(x, false)
    //   (1.0 - y) :* y
    // }
  }

  def tanh(x: DMD, deriv: Boolean = false): DMD = {
    if (!deriv) breeze.numerics.tanh(x)
    else {
      val y = breeze.numerics.sech(x)
      y :* y
    }
  }
}
