package mlp

import breeze.linalg.{Vector => _, _}
import breeze.numerics._
import breeze.stats.distributions.Gaussian
import breeze.generic._


final class Layer(size: Int,
              sizeNext: Int,
         minibatchSize: Int,
            activation: (DenseMatrix[Double], Boolean) => DenseMatrix[Double],
               isInput: Boolean = false,
              isOutput: Boolean = false) {
  //B
  val biases =
    DenseMatrix.rand(minibatchSize, sizeNext, Layer.dist(size))
  //Z;X
  val values = DenseMatrix.zeros[Double](minibatchSize, size)
  //W
  //
  //Each weight matrix corresponds to the next layer.
  //Therefore, input layer has weights, while output doesn't
  //
  //TODO: check can be removed
  val weights =
    if (isOutput) DenseMatrix.zeros[Double](0,0)
    else DenseMatrix.rand(size, sizeNext, Layer.dist(size))

  //S, weighted
  val inputs =
    if (isInput) DenseMatrix.zeros[Double](0,0)
    else DenseMatrix.zeros[Double](minibatchSize, size)
  //D
  val deltas =
    if (isInput) DenseMatrix.zeros[Double](0,0)
    else DenseMatrix.zeros[Double](size, minibatchSize)//(minibatchSize, size)
  //Fp
  val derivatives =
    if(isInput || isOutput) DenseMatrix.zeros[Double](0,0)
    else DenseMatrix.zeros[Double](size, minibatchSize)

  def activate(): DenseMatrix[Double] = {
    if(isInput) (values * weights) + biases
    else {
      values := activation(inputs, false)
      if(isOutput) values
      else {
        derivatives := activation(inputs, true).t
        (values * weights) + biases
      }
    }
  }
}

object Layer {
  def dist(size: Int) = Gaussian(0, pow(size, -0.5))
}
