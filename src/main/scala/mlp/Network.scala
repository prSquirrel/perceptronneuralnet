package mlp

import breeze.linalg.{Vector => _, _}
import breeze.numerics._
import spire.implicits.cfor


final class Network(layerConfig: List[Int], minibatchSize: Int, regression: Boolean) {
  //builds a list of all layers
  val (inputLayerActivation, outputLayerActivation, hiddenLayerActivation) =
    if (regression)
      (Activation.ctanh _, Activation.identity _, Activation.ctanh _)
    else
      (Activation.sigmoid _, Activation.softmax _, Activation.sigmoid _)

  val layers: Vector[Layer] =
    for (xs <- Util.window(layerConfig).toVector) yield xs match {
      case None :: Some(cur) :: Some(next) :: _ =>
        println(s"Initializing input layer with size $cur")
        new Layer(cur, next, minibatchSize, inputLayerActivation, isInput = true)
      case _ :: Some(cur) :: None :: _ =>
        println(s"Initializing output layer with size $cur")
        new Layer(cur, 0, minibatchSize, outputLayerActivation, isOutput = true)
      case _ :: Some(cur) :: Some(next) :: _ =>
        println(s"Initializing hidden layer with size $cur")
        new Layer(cur, next, minibatchSize, hiddenLayerActivation)
      case _ =>
        throw new IllegalArgumentException("List of layers cannot be empty.")
    }

  def forwardPropagate(batch: DenseMatrix[Double]): DenseMatrix[Double] = {
    layers.head.values := batch
    val last = layers.reduce { (cur: Layer, next: Layer) =>
      next.inputs := cur.activate()
      next
    }
    last.activate()
  }

  //todo: transform matrix logic to row-major instead of column-major
  def backPropagate(output: DenseMatrix[Double],
                    target: DenseMatrix[Double]) = {
    val inLayer +: hiddenLayers :+ outLayer = layers
    outLayer.deltas := (output - target).t
    layers.tail.reverseIterator.sliding(2).foreach {
      case List(cur, prev) => // http://neuralnetworksanddeeplearning.com/chap3.html CROSS ENTROPY ERROR FUNC IF NO DERIVATIVES ARE USED
        prev.deltas := (prev.weights * cur.deltas) :* prev.derivatives
    }
    ()
  }

  def updateWeights(learningRate: Double): Unit = {
    val last = layers.reduce { (cur: Layer, next: Layer) =>
      val wGrad =
        -learningRate :* (next.deltas * cur.values).t
      val bGrad =
        -learningRate :* next.deltas.t
      cur.weights :+= wGrad
      cur.biases :+= bGrad
      next
    }
    ()
  }

  def evaluate(train: IndexedSeq[(DenseMatrix[Double], DenseMatrix[Double])],
               test: IndexedSeq[(DenseMatrix[Double], DenseMatrix[Double])],
               epochs: Int = 500,
               learningRate: Double = 1.0 / sqrt(minibatchSize), //http://stackoverflow.com/questions/13693966/neural-net-selecting-data-for-each-mini-batch
               shuffle: Boolean = true,
               evalTrain: Boolean = false,
               evalTest: Boolean = true) = {
    val both = train.zip(test)
    println(s"Training for $epochs epochs...")
    cfor(1)(_ < epochs + 1, _ + 1) { t =>
      for (((trainIn, trainOut), (testIn, testOut)) <- both) {
        val (data, target) =
          if (!shuffle) (trainIn, trainOut)
          else Util.shuffleData(trainIn, trainOut)
        val output = forwardPropagate(data)
        backPropagate(output, target)
        updateWeights(learningRate)

        if (evalTrain) {
          val err = output - target
          println(s"[$t]Training error: ${sum(err :* err) / (minibatchSize * err.cols)}")
        }

        if (evalTest) {
          val tOutput = forwardPropagate(testIn)
          val err = tOutput - testOut
          println(s"[$t]Test error: ${sum(err :* err) / (minibatchSize * err.cols)}")
        }
      }
    }
    this
  }
}

object Network {

  import java.nio.file.{Files, Paths}

  import scala.pickling.Defaults._
  import scala.pickling._
  import scala.util.Try

  //Scala's implicit resolution thinks we have a recursive implicit
  //and stops resolution after being deep enough.
  //This splits the work into several stages
  implicit val layerPickler = Pickler.generate[Layer]
  implicit val layerUnpickler = Unpickler.generate[Layer]
  implicit val networkPickler = Pickler.generate[Network]
  implicit val networkUnpickler = Unpickler.generate[Network]

  def pickleToJSON(network: Network, filename: String): Try[Unit] = Try {
    import scala.pickling.json._
    val networkPickle = network.pickle
    Files.write(Paths.get(filename), networkPickle.value.getBytes())
  }

  def unpickleFromJSON(filename: String): Try[Network] = Try {
    import scala.pickling.json._
    val raw = Files.readAllBytes(Paths.get(filename))
    val lines = new String(raw)
    JSONPickle(lines).unpickle[Network]
  }

  def pickleToBinary(network: Network, filename: String): Try[Unit] = Try {
    import scala.pickling.binary._
    val networkPickle = network.pickle
    Files.write(Paths.get(filename), networkPickle.value)
  }

  def unpickleFromBinary(filename: String): Try[Network] = Try {
    import scala.pickling.binary._
    val raw = Files.readAllBytes(Paths.get(filename))
    BinaryPickle(raw).unpickle[Network]
  }
}
