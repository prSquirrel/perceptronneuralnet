package mlp

import breeze.linalg.{Vector => _, _}
import util.Random
import collection.breakOut


object Util {
  def shuffleData(data: DenseMatrix[Double], target: DenseMatrix[Double]) = {
    val extraCols = target.cols
    val augm = DenseMatrix.horzcat(data, target)
    val sh = shuffleRows(augm)
    ( sh(::, 0 until sh.cols - extraCols) , sh(::, -extraCols to sh.cols - 1) )
  }

  def shuffleRows(dm: DenseMatrix[Double]): DenseMatrix[Double] = {
    val rows = dm.rows
    val cols = dm.cols
    val arr = dm.toArray
    val r = Random.shuffle[Int, IndexedSeq](0 until rows)
    val shuffledArr: Array[Double] =
      (for(i <- 0 until cols; j <- r) yield arr(i * rows + j))(breakOut)
    new DenseMatrix(rows, cols, shuffledArr)
  }

  def window[A](l: List[A]): Iterator[List[Option[A]]] =
   (None :: l.map(Some(_)) ::: List(None)) sliding 3

}
